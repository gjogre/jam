﻿using UnityEngine;
using System.Collections;

public class VentigoStart : MonoBehaviour {
	private SpriteRenderer sr;
	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		sr.material.SetColor ("_Color", 
			new Color (
				Random.Range (2f, 80f)/100f, 
				Random.Range (2f, 80f)/100f, 
				Random.Range (2f, 80f)/100f));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
