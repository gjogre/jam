﻿using UnityEngine;
using System.Collections;

public class VendigoMovement : MonoBehaviour {

	public LayerMask ground;
	public Transform groundChecker;
	public Transform leftGroundChecker;
	public Transform rightGroundChecker;
	public float groundCheckRadius = 0.1f;
	private bool grounded;
	private bool leftGrounded;
	private bool rightGrounded;
	private Transform playerPosition;
	private float distanceToPlayer;

	public enemyState activeState;

	private Rigidbody2D rb;
	private StateManager gameState;

	private float randomizedMoveSpeed = 1;

	private Animator anim;

	public enum enemyType{
		idiot,
		aggressive,
		medium
	};
	public enemyType myEnemyType;

	public enum enemyState{
		inactive,
		passive,
		aggressive
	};


	void Start(){
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
		gameState = GameObject.Find("Game Manager").GetComponent<StateManager>();
		SwitchState(enemyState.inactive);
		StartCoroutine(RerollMoveSpeed());
		playerPosition = GameObject.Find("Player").transform;

		if(myEnemyType == enemyType.idiot){
			anim.Play("mato_cat_walk");
		}
		if(myEnemyType == enemyType.aggressive){
			anim.Play("Bull_mentula_cat_walk");
		}
		if(myEnemyType == enemyType.medium){
			anim.Play("Skinny_enemy_walk");
		}
	}
	
	void FixedUpdate(){
		distanceToPlayer = Vector2.Distance(transform.position, playerPosition.position);
		grounded = Physics2D.OverlapCircle(groundChecker.position, groundCheckRadius, ground);
		leftGrounded = Physics2D.OverlapCircle(leftGroundChecker.position, groundCheckRadius, ground);
		rightGrounded = Physics2D.OverlapCircle(rightGroundChecker.position, groundCheckRadius, ground);
	}

	public void SwitchState(enemyState newState)
	{
		activeState = newState;
		
		switch (activeState){
		default:
		case enemyState.inactive:
			StartCoroutine(Inactive());
			break;
		case enemyState.passive:
			StartCoroutine(Passive());
			break;
		case enemyState.aggressive:
			StartCoroutine(Aggressive());
			break;
		}
	}

	IEnumerator RerollMoveSpeed(){
		randomizedMoveSpeed = Random.Range(0.0f, 1);
		yield return new WaitForSeconds(Random.Range(2f, 5f));
		StartCoroutine(RerollMoveSpeed());
	}

	IEnumerator Inactive(){
		while(gameState.gameState == StateManager.State.BobTheBuilder){
			rb.velocity = new Vector2(0, 0);
			yield return null;
		}
		SwitchState(enemyState.passive);
	}

	bool movingLeft = false;
	IEnumerator Passive(){
		while(activeState == enemyState.passive){

			if(rightGrounded && !movingLeft){
				transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x + randomizedMoveSpeed, transform.position.y), 2 * Time.deltaTime);
			} else if(!rightGrounded && !movingLeft){
				movingLeft = true;
			}
			if(leftGrounded && movingLeft){
				transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x - randomizedMoveSpeed, transform.position.y), 2 * Time.deltaTime);
			}else if(!leftGrounded && movingLeft){
				movingLeft = false;
			}
			if(distanceToPlayer < 10 && myEnemyType == enemyType.aggressive){
				SwitchState(enemyState.aggressive);
			}
			yield return null;
		}
	}


	IEnumerator Aggressive(){
		while(activeState == enemyState.aggressive){
			
			if(rightGrounded && !movingLeft){
				transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x + 3, transform.position.y), 2 * Time.deltaTime);
			} else if(!rightGrounded && !movingLeft){
				movingLeft = true;
			}
			if(leftGrounded && movingLeft){
				transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x - 3, transform.position.y), 2 * Time.deltaTime);
			}else if(!leftGrounded && movingLeft){
				movingLeft = false;
			}
			if(distanceToPlayer > 15){
				SwitchState(enemyState.passive);
			}
			yield return null;
		}
	}
}














