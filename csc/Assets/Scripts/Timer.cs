﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public double theTime;

	private bool paused = false;

	public bool isPassed(double seconds){
		return (Time.time - theTime) > seconds;
	}

	public void startTimer(){
		paused = false;
		theTime = Time.time;
	}

	public void pause(){
		paused = true;
	}

	public bool isPaused(){
		return paused;
	}
}
