﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapGenerator : MonoBehaviour {

	public GameObject PlatformPrefab;
	public GameObject[] VendigoPrefab;
	public GameObject DrumPrefab;

	public int platformMinWidth = 2;
	public int platformMaxWidth = 4;


	public float vendigoSpawnOffsetY = 1;

	private StateManager gameState;

	public int platformsLeft = 3;

	public Camera shitcamera;


	private List<Platform> GeneratePlatforms(int amount, Vector2 bounds, Vector2 offset) {


		List<Platform> platforms = new List<Platform>();

		int y = 0;
		int tempX = 0;
		for (int i = 0; y < bounds.y; i++) {
			Platform newPlat = new Platform ();
			newPlat.x = Random.Range ((int)tempX + (int)offset.x, (int)bounds.x - tempX);
			newPlat.y = -y - offset.y;
			newPlat.width = Random.Range (platformMinWidth, platformMaxWidth);
			//Debug.Log (Random.Range (0, 2));
			if (newPlat.x < bounds.x / 2) {
				
				if (Random.Range (0, 2) == 1) {
					tempX =  3;
				} else {
					y += 3;
					tempX = 0;
				}
			
			} else {
				y += 3; 
				tempX = 0;
			}
			platforms.Add (newPlat);
		}

		return platforms;
	}

	private List<GameObject> GenerateVendigos(List<Platform> platforms) {
	
		int stupidIndex = Random.Range (0, platforms.Count);
		int aggressiveIndex;
		do {
			aggressiveIndex = Random.Range (0, platforms.Count);
		} while(aggressiveIndex == stupidIndex);

		int jumperIndex;
		do {
			jumperIndex = Random.Range (0, platforms.Count);
		} while(jumperIndex == stupidIndex || jumperIndex == aggressiveIndex);

		List<GameObject> vendigos = new List<GameObject> ();

		vendigos.Add((GameObject)Instantiate(
			VendigoPrefab[0], 
			new Vector3(platforms.ElementAt(stupidIndex).x, 
				platforms.ElementAt(stupidIndex).y + vendigoSpawnOffsetY), 
				Quaternion.identity));
		vendigos.Add((GameObject)Instantiate(
			VendigoPrefab[1], 
			new Vector3(platforms.ElementAt(aggressiveIndex).x, 
				platforms.ElementAt(aggressiveIndex).y + vendigoSpawnOffsetY), 
			Quaternion.identity));
		vendigos.Add((GameObject)Instantiate(
			VendigoPrefab[2], 
			new Vector3(platforms.ElementAt(jumperIndex).x, 
				platforms.ElementAt(jumperIndex).y + vendigoSpawnOffsetY), 
			Quaternion.identity));


		return vendigos;
	}


	public void Start() {
		gameState = GetComponentInParent<StateManager> ();

		List<Platform> platforms = GeneratePlatforms (5, new Vector2 (40, 15), new Vector2(4, 2));

		GenerateVendigos (platforms);
		platforms.ForEach (delegate(Platform plat) {



			for(int i = 0; i < plat.width; i++) {
				plat.gameObjects.Add( (GameObject)Instantiate(
					PlatformPrefab, 
					new Vector3(plat.x+i, plat.y , 0), 
					Quaternion.identity));	
			}

		});

	}


	public void Update() {
		

		if (platformsLeft > 0) {
			if (Input.GetKeyDown (KeyCode.Mouse0)) {
				Vector3 mousePos = shitcamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x ,
					Input.mousePosition.y ,Camera.main.transform.position.z));
				mousePos.x = -mousePos.x+34;
				mousePos.y = -mousePos.y +4;
				mousePos.z = 0;
				Debug.Log (mousePos);
				Instantiate (DrumPrefab, mousePos, Quaternion.identity);
				platformsLeft--;
				if (platformsLeft <= 0) {
					
					StartCoroutine (piss ());

				}
			
			}
		
		
		}
	
	
	}


	private class Platform {
		public float x;
		public float y;
		public int width;
		public List<GameObject> gameObjects = new List<GameObject>();
	}


	IEnumerator piss(){
		yield return new WaitForSeconds(1f);
		gameState.gameState = StateManager.State.CatShamanCannon;
	}	
}
