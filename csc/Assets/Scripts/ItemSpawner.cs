﻿using UnityEngine;
using System.Collections;

public class ItemSpawner : MonoBehaviour {


	[SerializeField]
	Vector3[] spawnLocations;
	[SerializeField]
	GameObject[] spawnableItems;

	int lastSpawnLocation;


	void SpawnItem(){
		Instantiate(spawnableItems[GetRandomNumber(spawnableItems.Length)], spawnLocations[GetRandomNumber(spawnLocations.Length)], Quaternion.identity);
	}

	int GetRandomNumber(int max, int min = 0){
		int number = Random.Range(min, max);
		return number;
	}
}
