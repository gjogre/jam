﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

	public int MAX_MOVEMENT_MULTIPLIER = 3;
	public float MOUSE_PRESS_DELAY = 1; //seconds

	[SerializeField]
	Vector2 moveForceLeft;
	[SerializeField]
	Vector2 moveForceRight;
	public bool grounded;
	public LayerMask ground;
	[SerializeField]
	float groundCheckRadius;
	[SerializeField]
	Transform groundChecker;
	[SerializeField]
	Image multiplierSlider;

	private float scale;
	private bool hopping = false;
	
	//private SpriteRenderer rend;
	private Animator anim;
	private Rigidbody2D rb;

	private Timer timer;
	//Timer stuff
	private float forceMultiplier;

	bool leftClicked = false;
	bool rightClicked = false;

	public int lives = 9;

	public AudioClip dieSound;

	AudioSource audio;


	public GameObject gameManager;
	public StateManager gameState;


	void Start(){
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
		scale = transform.localScale.x;
		timer = new Timer();
		multiplierSlider.fillAmount = 0;

		scale = transform.localScale.x;

		audio = gameObject.AddComponent<AudioSource >();
		audio.playOnAwake = false;

		gameManager = GameObject.Find ("Game Manager");
		gameState = gameManager.GetComponent<StateManager> ();
		anim.Play("Player_Idle");
	}
	

	void Update(){
		if (gameState == null) {
			gameState = gameManager.GetComponent<StateManager> ();
		}
		if (gameState.gameState == StateManager.State.CatShamanCannon) {
			grounded = Physics2D.OverlapCircle(groundChecker.position, groundCheckRadius, ground);
			if(!grounded || hopping){
				anim.Play("Player_Air");
				return;
			}
			if(Input.GetKey(KeyCode.Mouse0) && !rightClicked){
				if(!leftClicked){
					leftClicked = true;
				}
				mouseDown(-scale);
			}
			else if(Input.GetKeyUp(KeyCode.Mouse0) && !rightClicked){
				mouseUp(moveForceLeft);
			}
			else if(Input.GetKey(KeyCode.Mouse1) && !leftClicked){
				if(!rightClicked){
					rightClicked = true;
				}
				mouseDown(scale);
			}
			else if(Input.GetKeyUp(KeyCode.Mouse1) && !leftClicked){
				mouseUp(moveForceRight);
			}
			else if (grounded){
				rb.velocity = new Vector2(0, 0);
			}
				
			anim.Play("Player_Idle");
		}
	}

	IEnumerator Move(Vector2 moveForce){
		rb.AddForce(moveForce, ForceMode2D.Impulse);
		hopping = true;
		yield return new WaitForSeconds(0.05f);
		hopping = false;
	}	
	

	private void mouseDown(float _scale){
		if(timer.isPaused()){
			timer.startTimer();
			transform.localScale = new Vector3(_scale,transform.localScale.y,transform.localScale.z);
		}
		if(forceMultiplier <= MAX_MOVEMENT_MULTIPLIER){
			if(timer.isPassed(MOUSE_PRESS_DELAY)){
				forceMultiplier += 1;
				timer.startTimer();
				multiplierSlider.fillAmount += 1.0f / (MAX_MOVEMENT_MULTIPLIER);
			}
		}
	}
	
	private void mouseUp(Vector2 moveDirection){
		timer.pause();
		StartCoroutine(Move(moveDirection * forceMultiplier));
		multiplierSlider.fillAmount = 0;
		forceMultiplier = 1;
		resetClicked();
	}

	private void resetClicked(){
		if(leftClicked){leftClicked = false;}
		if(rightClicked){rightClicked = false;}
	}

	public void die() {
		audio.PlayOneShot (dieSound);
		UIScript.killCat ();
		
	}
}
