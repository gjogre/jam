﻿using UnityEngine;
using System.Collections;

public class DrumLogic : MonoBehaviour {


	private GameObject visitor;
	private SpriteRenderer sr;

	private Vector3 dragDistance;
	bool dragging = false;
	bool stay = false;

	public float force = 2f;

	public LineRenderer line;

	public AudioClip shoot1;
	public AudioClip shoot2;
	public AudioClip drum;
	AudioSource audio;
	public ParticleSystem bardigles;

	private
	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		line = GameObject.Find ("Line").GetComponent<LineRenderer> ();
		audio = gameObject.AddComponent<AudioSource >();
		audio.playOnAwake = false;


	}

	// Update is called once per frame
	void Update () {
		if (visitor != null && stay) {
			visitor.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
			visitor.gameObject.transform.position = transform.position;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			audio.PlayOneShot(drum);
			bardigles.Play ();
			stay = true;
			visitor = coll.gameObject;
			sr.material.SetColor ("_Color", 
				new Color (2f, 
					2f, 
					2f));
		
		}
	}
	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			visitor = null;
			sr.material.SetColor ("_Color", 
				new Color (1f, 
					1f, 
					1f));
		
		}
	

	}

	void OnMouseUp() {
		if (dragging) {
			dragging = false;
			stay = false;
			line.SetPositions (new Vector3[] { new Vector3(-100,-100,0), new Vector3(-100,-100,0) });
			Debug.Log("Drag ended!");
			visitor.gameObject.GetComponent<Rigidbody2D>().AddForce(dragDistance * force, ForceMode2D.Impulse);
			audio.PlayOneShot(shoot1);
			audio.PlayOneShot(shoot2);

		}

	}

	void OnMouseDrag() {
		if (visitor != null) {
			dragging = true;
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x ,
				Input.mousePosition.y ,Camera.main.transform.position.z));
			mousePos.x = -mousePos.x+34;
			mousePos.y = -mousePos.y +4;
			mousePos.z = 0;
			dragDistance = visitor.transform.position - mousePos;





			Vector3 shit = transform.position;


			float a = shit.x - mousePos.x;
			float b = shit.y - mousePos.y;
			float angle = Mathf.Atan2(-b, -a) * Mathf.Rad2Deg;

		
			transform.rotation = Quaternion.Euler(0, 0, angle + 90);
	






			line.SetPositions (new Vector3[] { transform.position, transform.position+dragDistance });
			//Debug.Log (dragDistance);

		}
	}


}
