﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public Texture tex;

	private static int lives = 9;
	// Use this for initialization
	void Start () {


	}

	public static void killCat() {
	
		lives--;

		if (lives <= 0) {
			Application.LoadLevel ("Menu");
		}
	}



	void OnGUI(){
		Rect r = new Rect(Screen.width-800,Screen.height - 100, Screen.width, Screen.height); //Adjust the rectangle position and size for your own needs
		GUILayout.BeginArea(r);
		GUILayout.BeginHorizontal();

		for(int i = 0; i < lives; i++)
			GUILayout.Label(tex); //assign your heart image to this texture

		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
}
