﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UiHandler : MonoBehaviour {
	public Text HelpBox;
	private StateManager stateManager;
	private MapGenerator mg;
	private GameObject panel;

	void Start() {
		stateManager = GameObject.Find ("Game Manager").GetComponent<StateManager> ();
		mg = GameObject.Find ("SceneBuilder").GetComponent<MapGenerator> ();
		panel = GameObject.Find ("helpPanel");
	}

	void Update() {
	
		if (stateManager.gameState == StateManager.State.BobTheBuilder) {
			HelpBox.text = "BUILDING STATE: You got " + mg.platformsLeft + " drums left. Click on empty spaces you think needs them";
			
		} else if (stateManager.gameState == StateManager.State.CatShamanCannon) {
			panel.SetActive (false);
		}

	
	}
}
