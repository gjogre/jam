﻿using UnityEngine;
using System.Collections;

public class StateManager : MonoBehaviour {

	public enum State {
		Menu = 0,
		BobTheBuilder = 1,
		CatShamanCannon = 2
	}

	public State gameState = State.BobTheBuilder;


	public void Update() {
	
		//Debug.Log (gameState);
	}

}
